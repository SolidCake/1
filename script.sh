# 1 #

fdisk -l

# fdisk /dev/sda -> n -> default -> default -> +300M -> w

# 2 #

sudo blkid /dev/sda3 > UUID.txt

# 3 #

fsck.ext4 /dev/sda3
mkfs.ext4 -b 4096 /dev/sda3

# 4 #

dump2fs /dev/sda3 > file_sys_info.txt

# 5 #

tune2fs -c 0 /dev/sda3
tune2fs -c 2 /dev/sda3
tune2fs -i 2m /dev/sda3

# 6 #

mkdir /mnt/newdisk
sudo mount /dev/sda3 /mnt/newdisk

# 7 #

ln -s /mnt/newdisk/lost+found link

# 8 #

mkdir /mnt/newdisk/lost+found/task_8_catalog

# 9 #

echo "/dev/sda3 /mnt/newdisk ext4 noexec,noatime 0 1" >> /etc/fstab

# 10 #

umount /dev/sda3
# fdisk /dev/sda -> d -> 3 -> n -> default -> default -> +350M -> w

# 11 #

fsck.ext4 -n /dev/sda3

# 12 #

# fdisk /dev/sda -> n -> default -> default -> +12M -> w
tune2fs -j -J device=/dev/sda4 /dev/sda3

# 13 #

# fdisk /dev/sda -> n -> default -> default -> +100M -> n -> default -> default -> w

# 14 #

pvcreate /dev/sdb1 /dev/sdb/2
lvcreate -L 200M -n logical_volume LVM_Group
mkfs.ext4 /dev/LVM_Group/logical_volume
mkdir /mnt/supernewdisk
mount /dev/LVM_Group/logical_volume /mnt/supernewdisk

# 15 #

mkdir /mnt/share
mount.cifs -o username=username,password=password //IP /mnt/share

# 16 #

echo "//IP/shared /mnt/share ext4 cifs ro 0 0" >> /etc/fstab
